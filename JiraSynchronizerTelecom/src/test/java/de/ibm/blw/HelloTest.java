/**
 * 
 */
package de.ibm.blw;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author adpadmin
 *
 */
public class HelloTest {

	@Test
	public void test() {
		Hello hello = new Hello();
		assertTrue(hello.sayXmlHello().contains("Telecom"));
		assertTrue(hello.sayHtmlHello().contains("Telecom"));
	}

}
