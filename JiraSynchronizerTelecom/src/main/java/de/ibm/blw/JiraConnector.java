package de.ibm.blw;

import java.net.URI;
import java.util.Base64;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;

public class JiraConnector {
	private String credentials = "<username>:<password>";
	
	JiraConnector(){
		System.setProperty("https.protocols", "TLSv1.2");
	}

    public String getConnection() {
    	this.addComment();
        /*ClientConfig config = new ClientConfig();
        Client client = ClientBuilder.newClient(config);
        WebTarget target = client.target(getBaseURI());
        // Get JSON for application
        String credentials = "<username>:<password>";
        String base64encoded = Base64.getEncoder().encodeToString(credentials.getBytes());
        String jsonResponse = target.request()
        		.header(HttpHeaders.AUTHORIZATION, "Basic " + base64encoded)
                .accept(MediaType.APPLICATION_JSON).get(String.class);
        System.out.println(jsonResponse);
        return jsonResponse;*/
    	return "HI";
    }
    
    public String getIssue(String id) {
        Client client = ClientBuilder.newClient();
        WebTarget service = client.target("https://dth04.ibmgcloud.net/jira/rest/api/latest/issue/PLAYG-"+id);
        String base64encoded = Base64.getEncoder().encodeToString(this.credentials.getBytes());
        String response = service.request()
        		.header(HttpHeaders.AUTHORIZATION, "Basic " + base64encoded)
        		.accept(MediaType.APPLICATION_JSON).get(String.class);
        System.out.println("Response " + response);
        return response;
    }

    private static URI getBaseURI() {
        return UriBuilder.fromUri(
                //"https://en.wikipedia.org/api/rest_v1/page/").build();
        		"https://dth04.ibmgcloud.net/jira/rest/api/latest/issue/PLAYG-146/comment").build();
    }
    
    private void addComment(){
    	ClientConfig config = new ClientConfig();
        Client client = ClientBuilder.newClient(config);
        WebTarget service = client.target(getBaseURI());
        String credentials = "<username>:<password>";
        String base64encoded = Base64.getEncoder().encodeToString(credentials.getBytes());
        Response response = service.request()
        		.header(HttpHeaders.AUTHORIZATION, "Basic " + base64encoded)
        		.post(Entity.entity("{\"body\": \"Test comment for Maddy!!\"}",MediaType.APPLICATION_JSON),Response.class);
        System.out.println("Response " + response.getStatus());
    }
}